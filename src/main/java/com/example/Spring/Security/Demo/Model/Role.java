package com.example.Spring.Security.Demo.Model;


public enum Role {
    USER,
    ADMIN
}